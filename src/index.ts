import express from "express";
import * as http from "http";
import fs from 'fs';
import pathResolver from "path";
const app = express()


function bufferVideo(range: string, fileSize: number, path: string): [any, fs.ReadStream]{
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1]
        ? parseInt(parts[1], 10)
        : fileSize - 1
    const chunksize = (end - start) + 1
    const file = fs.createReadStream(path, { start, end })
    return [{
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
    }, file]
}


app.get('/stream', (req, res) => {
    const uri = `http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4`
    // const path = '/home/bunny/assets/sample2.mp4'
    const relative = './assets'

    // TODO create dir assets if not exists.
    const dir = pathResolver.resolve(__dirname, relative)
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    const path = dir + '/sample2.mp4'
    const fileToSave = fs.createWriteStream(path);
    http.get(uri,  (responseGET) => {
        responseGET.pipe(fileToSave).on('finish', () => {
            fileToSave.close()
            fs.readFile(path, (err: NodeJS.ErrnoException, data: Buffer) => {
                const fileSize = fs.statSync(path).size
                const range = req.headers.range
                if (range) {
                    const [head, file] = bufferVideo(range, fileSize, path)
                    res.writeHead(206, head);
                    file.pipe(res);
                } else {
                    const head = {
                        'Content-Length': fileSize,
                        'Content-Type': 'video/mp4',
                    }
                    res.writeHead(200, head)
                    fs.createReadStream(path).pipe(res)
                }
                res.status(200)
            })
        })
    }).on('error', (err) => {
        console.log("Something went wrong");
        console.log(err);
    });;
    console.log("function end.")
});

app.listen(7321, () => {
    console.log("Init")
})

