FROM node:10.13.0-alpine

WORKDIR /usr/src/app

COPY package.json .
RUN npm install
RUN npm run prestart

CMD ["npm" "start"]
EXPOSE 7321
